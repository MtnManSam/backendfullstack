HTML - HyperText Markup Language
  - made of:
    * text content (what you see)
	* markup (what it looks like)
	* references to other documents
	* lnks to other pages
	
HTML Markup
  - <NAME> Contents </NAME>
      ^                ^
	opening          closing
	  tag              tag
	
	------element------------
	
Bold tag:
  - <b>contents</b>
em (italics) tag:
  - <em>contents</em>
  
HTML Attributes:
  - <TAG ATTR="value">contents</TAG>
  - Anchor - <a>
    - <a href="www.reddit.com">derp</a>
	      ^          ^  
		attribute    value
		  name
		  
Image tag:
  - <img> - images
  - <img src="url" alt="text">
                     ^
			        required,for
					broken requests...
  - no closing tag!
    - called a Void Tag

New lines tags:
  - Whitespace
    - <br> (void tag)
     - break tag, inline
	 - simply ends a line

  - Paragraph tag:
    - <p>contents</p>
    - blocks in text
	  - block size has height and width
 
Inline elements:
  - <b>, <em>, <img>
  
Block elements:
  - <p>
  
Span and Div tags:
  - <span class="foo">          <div class="bar">
      text                      text
	</span>                     </div>
	 inline                      block

URLs
  - Query parameters (GET parameters)
    - NAME=VALUE
    - indicated by a ? mark
      - e.g. http://example.com/foo?P=1&g=neat
      - all others indicated by a & mark
	
  - Fragments (not sent to server, purely in browser)
    - denoted with #
	- e.g. http://example.com/#fragment
	
  - Port
    - location of machine requested
	- e.g. http://localhost:8000/
	                        port-default = 80

  - HTTP (HyperText Transfer Protocol)
    - e.g. http://www.example.com/foo
	- GET /foo HTTP/1.1 - request line
	  ^     ^    ^
   method  path  version
    GET
	POST
	
User agent
  - helpful indicators of who you are on the web
  
HTTP Responses
  - e.g. request: GET /foo HTTP/1.1
         response: HTTP/1.1 200 OK
		                     ^   ^
						  status reason
						   code  phrase
  - 200 OK
    302 Found
	404 Not Found
	600 Server Error
  - number indicators: 2- all good, 3- need more, 4- dunno, 6- server error
  
	 
  - e.g. HTTP response: HTTP/ 1.1 200 OK
                        Server: apache/2.2.3 (HIDE THIS INFO!!!!)
						Content-Type: text/html;
						Content-Length: 1539
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 